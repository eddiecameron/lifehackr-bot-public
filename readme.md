@LifehackrBot
A bot employed by Gawker to keep their post count up.

Based heavily on [@TwoHeadlines](https://github.com/dariusk/twoheadlines), by [Darius Kazemi](http://tinysubversions.com)

Harvests post titles from lifehacker, combined by tag.
Makes a Markov chain corpus from each of two post tags, and tries to markov generate a headline that starts in one tag's corpus and ends in another.

##Instructions 

Install [node](http://nodejs.org/)

Replace the appropriate fields in config.js with your own Twitter API info (or your bot's own account!)

Run
$> node gatherer.js
To aharvest headlines from Lifehackr and store them in text files

Run
$> node index.js
To generate a twet based on the headlines from two different tags

Success!?

For more help, contact me, or take a look at Darius Kazemi's detailed walkthru of @twoheadlines, [here](http://tinysubversions.com/twoheadlines/docs/)

## License
Copyright Eddie Cameron 2013
[grapefruitgames.com](http://grapefruitgames.com) | [@eddiecameron](http://twitter.com/eddiecameron)

Licensed under the MIT license
