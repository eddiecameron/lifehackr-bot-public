// ### Libraries and globals

var request = require('request'); // for doing http requests (to lifehacker.com)
var cheerio = require('cheerio'); // for jquery-like parsing of the lifehackr pages
var fs = require( 'fs' ); // for writing headlines to files
var _ = require('underscore.deferred'); // for doing fancy callbacks

var diyTags = ['productivity', 'diy', '']; // big enouigh for their own files
var otherTags = ['relationships', 'happiness', 'psychology', 'quotables', 'mind-hacks', 'learning', 'parenting']; // need to combine I think

var baseUrl = 'http://lifehacker.com/';

// ### Screen Scraping

// This gets headlines from a list of lifehacker posts.
// pretty hacky lets hope they don't change the site layout again???!!!
function getTitlesAndNextPage(fromUrl) {
  var results = {};
  results.topics = [];
  var dfd = new _.Deferred();
  request(fromUrl, function (error, response, body) {
    if (!error && response.statusCode === 200) {
		var $ = cheerio.load(body);
		$('.h5').each(function() { // headlines are only h5 element on search result page
			results.topics.push( this.text() );
		});
		results.nextPage = $('.load-more-link').attr('href'); // lifehackers dynamic 'next-page' link thing
		dfd.resolve(results);
    }
    else {
		console.log( error );
      dfd.reject();
    }
  });
  // The function returns a promise, and the promise resolves to the array of topics.
  return dfd.promise();
}

function writeTitles( fromBaseURL, query, writeStream, titleCount, limit ) {
	var dfd = new _.Deferred();
	console.log( 'Getting from: ' + fromBaseURL + query );
	getTitlesAndNextPage( fromBaseURL + query ).then( function( results ) {
		for ( var i = 0; i < results.topics.length; i++ ) {
			writeStream.write( results.topics[i] + '\n' );
		}
		titleCount += results.topics.length;
		if ( results.nextPage && titleCount < limit ) {
			writeTitles( fromBaseURL, results.nextPage, writeStream, titleCount, limit ).then( function(numTitles) {
				console.log( numTitles );
				return dfd.resolve(numTitles); 
			} );
		}
	} );
	return dfd.promise();
}

function harvestTitles(withTag, limit) {
	var url = baseUrl;
	if ( withTag ) {
		url += 'tag/' + withTag;
	}	
	console.log( 'Getting headlines from ' + url );
	
	// request headlines from URL, and write them one-per-line to a file of he same name
	var stream = fs.createWriteStream( 'headlines' + withTag );
	var dfd = new _.Deferred();
	stream.once( 'open', function() {
		writeTitles( url, '', stream, 0, limit ).then( function(numTitles) {
			stream.end();
			console.log( 'Found ' + numTitles + ' titles with tag: ' + withTag );
			return dfd.resolve();
		} );
	} );
	return dfd.promise();
}

// Fill out DB
for ( var i = 0; i < diyTags.length;i++ ) {
	harvestTitles( diyTags[i], 5000 );
}

for ( var i = 0; i < otherTags.length;i++ ) {
	harvestTitles( otherTags[i], 5000 );
}