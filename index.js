// ### Libraries and globals

var fs = require( 'fs' ); // for reading headline files
var Twit = require('twit'); // for tweeting
var T = new Twit(require('./config.js'));
var markov = require('markov');

// Parameters for the text generator
var markovOrder = 2; // the higher, the more words that are bunched together from the original text (more like original)
var minFirstTagWords = 1; // how many words before we can switch to generating from another tag
var markovTag1 = markov(markovOrder);
var markovTag2 = markov(markovOrder);

var diyTags = ['productivity', 'diy', '']; // big enouigh for their own files
var otherTags = ['relationships', 'happiness', 'psychology', 'quotables', 'mind-hacks', 'learning', 'parenting']; // need to combine I think

var fullHeadlines = []; // holds whole headlines to check results against (we don't want straight headlines)

// ### Utility Functions

// This function lets us call `pick()` on any array to get a random element from it.
Array.prototype.pick = function() {
  return this[Math.floor(Math.random()*this.length)];
};

// This function lets us call `pickRemove()` on any array to get a random element
// from it, then remove that element so we can't get it again.
Array.prototype.pickRemove = function() {
  var index = Math.floor(Math.random()*this.length);
  return this.splice(index,1)[0];
};

// from markov index.js
function cleanString(s) {
    return s
        .toLowerCase()
        .replace(/[^a-z\d]+/g, '_')
        .replace(/^_/, '')
        .replace(/_$/, '')
    ;
}

// ## Markov Chaining
// adapted from http://www.soliantconsulting.com/blog/2013/02/draft-title-generator-using-markov-chains
var startwords = [];

/// add given titles to a markov generation corpus
function addTitles(titles, markovCorpus, addStartWords){
	console.log( 'Seeding ' + titles.length + ' entries' );
	for (var i = 0; i < titles.length; i++) {
		markovCorpus.seed( titles[i] );
		var words = titles[i].split(' ');
		if ( addStartWords && words.length > markovOrder ) {
			startwords.push(words.slice(0, markovOrder).join( ' ' ) );
		}
	}
}

function makeTitle(min_length) {
	var startWord = startwords.pick();
    var cur = markovTag1.search( startWord );
	if ( !cur ) {
		console.log( 'No start word key found for ' + startWord );
		return '';
	}
	console.log( 'Start: ' + startWord );
	
	var firstTagWords = 0;
	var numWords = 0;
	var curMarkovEngine = markovTag1;
    var title = [startWord.charAt(0).toUpperCase() + startWord.slice(1)];
	while ( cur ) {
		var next = curMarkovEngine.next( cur );
		console.log( numWords );
		console.log( firstTagWords );
		if ( !next ) {
			if ( firstTagWords === 0 ) {
				numWords = minFirstTagWords;	// try switching to next tag
			}
			else if ( firstTagWords === numWords ) {
				return makeTitle( min_length );	// second tag was terminating
			}
			else {
				break;	// end of phrase
			}
		}
		else {
			// go to next word
			numWords++;
			console.log( next.word );
			cur = next.key;
			title.push( next.word );
		}

		if ( firstTagWords === 0 && numWords >= minFirstTagWords ) {
			// try switching to other tag
			var nextTagKey = markovTag2.search( cur );
			if ( nextTagKey ) {
				console.log( 'Switching to tag2' );
				firstTagWords = numWords;
				cur = nextTagKey;
				curMarkovEngine = markovTag2;
			}
			else if ( !next ) {
				console.log( 'Couldn\'t continue from last word in tag1' );
				return makeTitle( min_length );
			}
		}
	}
	
	var titleString = title.join(' ');
    if (title.length * markovOrder < min_length ) {
		console.log( 'Not long enough: ' + titleString );
		return makeTitle(min_length);
    }
	else { // check if just a straight headline
		var cleanTitle = cleanString( titleString );
		for ( var i = 0; i < fullHeadlines.length; i++ ) {
			if ( cleanString( fullHeadlines[i] ) === cleanTitle ) {
				console.log( 'Already a headline: ' + titleString );
				return makeTitle( min_length );
			}
		}
	}
	return titleString;
}

// ### Tweeting

function favRTs () {
  T.get('statuses/retweets_of_me', {}, function (e,r) {
    for(var i=0;i<r.length;i++) {
      T.post('favorites/create/'+r[i].id_str,{},function(){});
    }
    console.log('harvested some RTs'); 
  });
}

function tweet( tweetText ) {
	T.post('statuses/update', { status: tweetText }, function(err, reply) {
		if (err) {
		  console.log('error:', err);
		}
		else {
		  console.log('reply:', reply);
		}
	});
}

// We try making a markov chain headline??
// feed markov
function craftTweet() {
	var diyTag = diyTags.pick();
	var diyHeadlines = fs.readFileSync( 'headlines' + diyTag ).toString().split( '\n' );
	
	var otherTagHeadlines = [];
	for ( var i = 0; i < otherTags.length; i++ )
		otherTagHeadlines = otherTagHeadlines.concat( fs.readFileSync( 'headlines' + otherTags[i] ).toString().split( '\n' ) );
	
	fullHeadlines = [];
	fullHeadlines = fullHeadlines.concat( diyHeadlines ).concat( otherTagHeadlines );
	
	var diyAtStart = Math.random() > 0.5;
	if ( diyAtStart ) {
		fullHeadlines = diyHeadlines;
		addTitles( diyHeadlines, markovTag1, true );
		addTitles( otherTagHeadlines, markovTag2, false );
	}
	else {
		fullHeadlines = otherTagHeadlines;
		addTitles( otherTagHeadlines, markovTag1, true );
		addTitles( diyHeadlines, markovTag2, false );
	}
	console.log( 'Markov generated with tag ' + diyTag + ( diyAtStart ? ' at start' : ' at end' ) );
	var title = makeTitle( 5 );
	console.log( title );
	
	// Tweet it!
	tweet( title );
}

// Tweets every two hours.
craftTweet();
setInterval(function () {
  try {
	craftTweet();
  }
  catch (e) {
	console.log(e);
  }
}, 1000 * 60 * 60 * 2);

// every 5 hours, check for people who have RTed a metaphor, and favorite that metaphor
setInterval(function() {
  try {
    favRTs();
  }
 catch (e) {
    console.log(e);
  }
},60000*60*5);